#!/usr/bin/env sh

set -o errexit
set -o nounset
set -o pipefail

datadir="/var/lib/postgresql/${POSTGRESQL_CLUSTER_NAME}/"

if [ -d "${datadir}" ]; then
  echo "Skip initialization"
else
  csh "pg_ctl init --pgdata ${datadir}"
fi
