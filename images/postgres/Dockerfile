ARG BUILD_ARG_ALPINE_VERSION="3.19"
ARG BUILD_ARG_APP_GROUP_ID="65532"
ARG BUILD_ARG_APP_USER_ID="65532"

FROM alpine:${BUILD_ARG_ALPINE_VERSION} AS alpine
ARG BUILD_ARG_ALPINE_VERSION
ENV HISTFILE="/dev/null"
ENV TZ="UTC"
COPY ./images/postgres/stages/alpine/ /

FROM alpine AS postgresql-skeleton
ARG BUILD_ARG_APP_GROUP_ID
ARG BUILD_ARG_APP_USER_ID
COPY ./images/postgres/stages/postgresql-skeleton/ /
RUN /build.d/bin/install

FROM postgresql-skeleton AS postgresql-server-skeleton
COPY ./images/postgres/stages/postgresql-server-skeleton/ /
RUN /build.d/bin/install

FROM postgresql-server-skeleton AS postgresql-cluster-initializer
ARG BUILD_ARG_APP_GROUP_ID
ARG BUILD_ARG_APP_USER_ID
COPY ./images/postgres/stages/postgresql-cluster-initializer/ /
ENTRYPOINT ["/entrypoint.sh"]
RUN /build.d/bin/commit
USER ${BUILD_ARG_APP_USER_ID}:${BUILD_ARG_APP_GROUP_ID}
VOLUME ["/var/lib/postgresql/"]

FROM postgresql-server-skeleton AS postgresql-server
ARG BUILD_ARG_APP_GROUP_ID
ARG BUILD_ARG_APP_USER_ID
ENV POSTGRESQL_CLUSTER_NAME="stub"
COPY ./images/postgres/stages/postgresql-server/ /
RUN /build.d/bin/install
RUN /build.d/bin/commit
ENTRYPOINT ["/entrypoint.sh"]
USER ${BUILD_ARG_APP_USER_ID}:${BUILD_ARG_APP_GROUP_ID}
VOLUME ["/var/lib/postgresql/"]

FROM postgresql-skeleton AS postgresql-server-tls-initializer
ARG BUILD_ARG_APP_GROUP_ID
ARG BUILD_ARG_APP_USER_ID
COPY ./images/postgres/stages/postgresql-server-tls-initializer/ /
RUN /build.d/bin/install
RUN /build.d/bin/commit
ENTRYPOINT ["/entrypoint.sh"]
USER ${BUILD_ARG_APP_USER_ID}:${BUILD_ARG_APP_GROUP_ID}
VOLUME ["/etc/postgresql/tls/"]

FROM postgresql-server-skeleton AS postgresql-server-tls-watcher
ARG BUILD_ARG_APP_GROUP_ID
ARG BUILD_ARG_APP_USER_ID
ENV POSTGRESQL_CLUSTER_NAME="stub"
COPY ./images/postgres/stages/postgresql-server-tls-watcher/ /
RUN /build.d/bin/install
RUN /build.d/bin/commit
ENTRYPOINT ["/entrypoint.sh"]
USER ${BUILD_ARG_APP_USER_ID}:${BUILD_ARG_APP_GROUP_ID}
VOLUME ["/etc/postgresql/tls/"]
