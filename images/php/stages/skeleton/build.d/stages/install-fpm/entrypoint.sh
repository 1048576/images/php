#!/usr/bin/env bash

set -o errexit
set -o nounset
set -o pipefail

for script in $(find /entrypoint.d/bin/ -type f | sort); do
    csh "${script}"
done

exec "/usr/sbin/php-fpm${BUILD_ARG_PHP_VERSION}" "--nodaemonize"
