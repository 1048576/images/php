#!/usr/bin/env bash

set -o errexit
set -o nounset
set -o pipefail

function create_php_fpm_conf {
    local target_file="/etc/php/${BUILD_ARG_PHP_VERSION}/fpm/php-fpm.conf"
    local source_filepath="/entrypoint.d/templates${target_file}"

    csh "cp ${source_filepath} ${target_file}"
}

function create_pool_www_conf {
    local target_file="/etc/php/${BUILD_ARG_PHP_VERSION}/fpm/pool.d/www.conf"
    local source_filepath="/entrypoint.d/templates${target_file}"

    csh "cp ${source_filepath} ${target_file}"
}

create_php_fpm_conf
create_pool_www_conf
