#!/usr/bin/env bash

set -o errexit
set -o nounset
set -o pipefail

target_file="/etc/php/${BUILD_ARG_PHP_VERSION}/php.ini"
target_filepath="/entrypoint.d/templates${target_file}"

csh "cp ${target_filepath} ${target_file}"

creplace "${target_file}" "\\\${PHP_INI_MAX_EXECUTION_TIME}" "${PHP_INI_MAX_EXECUTION_TIME}"
creplace "${target_file}" "\\\${PHP_INI_MEMORY_LIMIT}" "${PHP_INI_MEMORY_LIMIT}"
creplace "${target_file}" "\\\${PHP_INI_POST_MAX_SIZE}" "${PHP_INI_POST_MAX_SIZE}"
creplace "${target_file}" "\\\${PHP_INI_UPLOAD_MAX_FILESIZE}" "${PHP_INI_UPLOAD_MAX_FILESIZE}"
creplace "${target_file}" "\\\${TZ}" "${TZ}"
