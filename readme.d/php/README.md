# php

- [./images/server/stages/php/build.d/bin/build](#./images/server/stages/php/build.d/bin/build)
- [./images/server/stages/server-dependencies/build.d/bin/build](#./images/server/stages/server-dependencies/build.d/bin/build)
- [./images/server/Dockerfile](#./images/server/Dockerfile)
- [./.env](#./.env)
- [./docker-compose.yml](#./docker-compose.yml)

```sh
cp ./server/.env.local.dist ./server/.env.local

docker-compose up
```

### <a id="./images/server/stages/php/build.d/bin/build"></a> ./images/server/stages/php/build.d/bin/build

```sh
#!/usr/bin/env bash

set -o errexit
set -o nounset
set -o pipefail

# .pre
csh "/build.d/bin/pre-install"

csh "/build.d/bin/install-php-extension ..."

# .post
csh "/build.d/bin/post-install"
csh "rm $0"
```

### <a id="./images/server/stages/server-dependencies/build.d/bin/build"></a> ./images/server/stages/server-dependencies/build.d/bin/build

```sh
#!/usr/bin/env bash

set -o errexit
set -o nounset
set -o pipefail

csh "/build.d/bin/composer install --no-scripts --no-dev"

# .post
csh "rm $0"
```

### <a id="./images/server/Dockerfile"></a> ./images/server/Dockerfile

```dockerfile
ARG BUILD_ARG_APP_VERSION="dev"
ARG BUILD_ARG_APP_GROUP_ID="65534"
ARG BUILD_ARG_APP_USER_ID="65534"
ARG BUILD_ARG_PHP_IMAGE_REGISTRY=""
ARG BUILD_ARG_PHP_IMAGE_TAG=""
ARG BUILD_ARG_COMPOSER_AUTH_JSON="{}"

FROM ${BUILD_ARG_PHP_IMAGE_REGISTRY}/php:${BUILD_ARG_PHP_IMAGE_TAG} AS php
COPY ./images/server/stages/php/ /
RUN /build.d/bin/build

FROM php AS fpm
RUN /build.d/bin/install-fpm

FROM php AS server-dependencies
ARG BUILD_ARG_COMPOSER_AUTH_JSON
COPY ./images/server/stages/server-dependencies/ /
COPY ./server/composer.json ./composer.json
COPY ./server/composer.lock ./composer.lock
RUN /build.d/bin/build

FROM php AS console
ARG BUILD_ARG_APP_VERSION
ARG BUILD_ARG_APP_GROUP_ID
ARG BUILD_ARG_APP_USER_ID
ENV APP_VERSION=${BUILD_ARG_APP_VERSION}
COPY --from=server-dependencies /app/vendor/ /app/vendor/
...
RUN /build.d/bin/commit
USER ${BUILD_ARG_APP_GROUP_ID}:${BUILD_ARG_APP_USER_ID}

FROM fpm AS site-backend
ARG BUILD_ARG_APP_VERSION
ARG BUILD_ARG_APP_GROUP_ID
ARG BUILD_ARG_APP_USER_ID
ENV APP_VERSION=${BUILD_ARG_APP_VERSION}
COPY --from=server-dependencies /app/vendor/ /app/vendor/
...
RUN /build.d/bin/commit
USER ${BUILD_ARG_APP_GROUP_ID}:${BUILD_ARG_APP_USER_ID}

FROM fpm AS server
ARG BUILD_ARG_APP_VERSION
ARG BUILD_ARG_APP_GROUP_ID
ARG BUILD_ARG_APP_USER_ID
ENV APP_VERSION=${BUILD_ARG_APP_VERSION}
RUN /build.d/bin/install-xdebug
RUN /build.d/bin/add-install-dependencies-script-on-running
...
RUN /build.d/bin/commit
USER ${BUILD_ARG_APP_GROUP_ID}:${BUILD_ARG_APP_USER_ID}
```

## <a id="./.env"></a> ./.env

```sh
cat << EOF > ./.env
BUILD_ARG_APP_GROUP_ID="$(id -g)"
BUILD_ARG_APP_USER_ID="$(id -u)"
COMPOSER_AUTH_JSON="{}"
XDEBUG_CLIENT_HOST=""
TZ="Europe/Kyiv"
SITE_PORT="8080"
EOF

# Linux
sed -i -e 's/XDEBUG_CLIENT_HOST=.*/XDEBUG_CLIENT_HOST="172.17.0.1"/' ./.env
# Mac OS
sed -i -e 's/XDEBUG_CLIENT_HOST=.*/XDEBUG_CLIENT_HOST="host.docker.internal"/' ./.env
```

### <a id="./docker-compose.yml"></a> ./docker-compose.yml

```yaml
services:
  site:
    image: "busybox:latest"
    command:
      - "sleep"
      - "infinity"
    ports:
      - "${SITE_PORT}:8080"
  server:
    build:
      args:
        BUILD_ARG_APP_GROUP_ID: "${BUILD_ARG_APP_GROUP_ID}"
        BUILD_ARG_APP_USER_ID: "${BUILD_ARG_APP_USER_ID}"
      context: "./"
      dockerfile: "./images/server/Dockerfile"
      target: "server"
    environment:
      COMPOSER_AUTH_JSON: "${COMPOSER_AUTH_JSON}"
      TZ: "${TZ}"
      XDEBUG_CLIENT_HOST: "${XDEBUG_CLIENT_HOST}"
    network_mode: "service:site"
    volumes:
      - "./server:/app:rw"
  site-frontend:
    ...
    network_mode: "service:site"
    ...
```
